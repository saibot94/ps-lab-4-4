#include "SubmenuItem.h"

/* TODO

    Use this function in order to validate if a user selection is correct or not.
    To read a user selection, write something like: cin >> option.
*/
bool SubmenuItem::valid_option(int i)
{
    if(i < 0 || (unsigned)i > menu_items.size()) {
        return false;
    }
    return true;
}

/* TODO

Implement this
*/
bool SubmenuItem::do_stuff()
{
    int option = 0;
    bool exit;
    while(!exit) {
        while(!valid_option(option-1)) {
            cout << "- " << get_name() << endl;
            cout << "==================" << endl;
            for(int i = 0; i < menu_items.size(); i++) {
                cout << i+1 << " ." << menu_items[i]->get_name()
                    << endl;
            }
            cout << translate("CHOOSE_OPTION_LABEL");
            cin >> option;
        }
        exit = menu_items[option-1]->do_stuff();
        option = 0;
    }
    return false;
 }
