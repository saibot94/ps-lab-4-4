#include "languages.h"

map<string, map<string, string>* >* language_map;
map<string, string>* read_language_file(string lang_code);


string current_language;

void change_language(string lang_code) {
    if(language_map == NULL) {
        language_map =
            new map<string, map<string, string>* >();
    }
    if(language_map->find(lang_code)==language_map->end())
    {
        (*language_map)[lang_code] =
            read_language_file(lang_code);
    }

    current_language = lang_code;
}

map<string, string>* read_language_file(string lang_code)
{
    map<string, string>* lang_map = new map<string, string>();
    string file_name = lang_code + string(".txt");
    ifstream f(file_name.c_str());
    string line;
    while(!f.eof()) {
        getline(f, line);
        int pos = line.find("=");
        if(pos != string::npos) {
            string key = line.substr(0, pos);
            string value = line.substr(pos+1);
            (*lang_map)[key] = value;
        }
    }
    f.close();
    return lang_map;
}


string translate(string label) {
    map<string, string>* current_lang_map =
        (*language_map)[current_language];

    map<string, string>::iterator found_translation =
        current_lang_map->find(label);

    if(found_translation == current_lang_map->end()) {
        return label;
    } else {
        return found_translation->second;
    }
}
