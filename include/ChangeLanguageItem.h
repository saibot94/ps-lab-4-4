#ifndef CHANGELANGUAGEITEM_H
#define CHANGELANGUAGEITEM_H
#include "MenuItem.h"

class ChangeLanguageItem : public MenuItem
{
    public:
        ChangeLanguageItem(string name) : MenuItem(name)
        {
        }
        virtual bool do_stuff();

    protected:

    private:
};

#endif // CHANGELANGUAGEITEM_H
