#pragma once
#include "projectlibs.h"

const string default_language = "ro";

void change_language(string lang_code);
string translate(string label);
