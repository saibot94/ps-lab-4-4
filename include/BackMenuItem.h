#ifndef BACKMENUITEM_H
#define BACKMENUITEM_H

#include "MenuItem.h"

class BackMenuItem : public MenuItem
{
    public:
        BackMenuItem(string name): MenuItem(name) {}
        virtual bool do_stuff();
};


#endif // BACKMENUITEM_H
